package vn.com.arilliance.studyinclass;

import android.app.ProgressDialog;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import vn.com.arilliance.studyinclass.controller.RestaurantDetailFragment;
import vn.com.arilliance.studyinclass.controller.ViewPagerAdapter;
import vn.com.arilliance.studyinclass.model.RestaurantModel;

public class MainActivity extends AppCompatActivity {

//    private MainAdapter adapter;
//
//    //Element Views
//    private EditText txtAddr, txtName;
//    private TextView txtMyId;
//    private Button btnSave, btnList, btnFilter;
//    private RadioGroup raTypesGroup, raDiscountGroup;
//    private RecyclerView recyclerView;
//
//    //Data holders
//    private List<RestaurantModel> restaurantList;

    private TabLayout tabLayout;
    private ViewPager viewPager;

    private ProgressDialog progressDialog; //Because old progressbar is deprecated, show we use this progressbar

    AtomicBoolean isActive = new AtomicBoolean(true);
    int progress = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        restaurantList = new ArrayList<>();
        initViews();
        initProgressBar();
    }

    private void initViews() {
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        viewPager = (ViewPager) findViewById(R.id.view_pager);

        tabLayout.addTab(tabLayout.newTab().setText("List"));
        tabLayout.addTab(tabLayout.newTab().setText("Detail"));
        tabLayout.addTab(tabLayout.newTab().setText("Sit down"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPagerAdapter adapter = new ViewPagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                adapter.notifyDataChanged(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private Map<String, Integer> getRestaurantTypesCount(List<RestaurantModel> list) {
        int takeout = 0, delivery = 0, sitdown = 0;
        for (RestaurantModel model : list) {
            if (model.getType().equalsIgnoreCase(RestaurantModel.TAKE_OUT)) {
                takeout++;
            } else if (model.getType().equalsIgnoreCase(RestaurantModel.DELIVERY)) {
                delivery++;
            } else {
                sitdown++;
            }
        }
        Map<String, Integer> countMap = new HashMap<>();
        countMap.put(RestaurantModel.TAKE_OUT, takeout);
        countMap.put(RestaurantModel.DELIVERY, delivery);
        countMap.put(RestaurantModel.SIT_DOWN, sitdown);
        return countMap;
    }

    private void initProgressBar(){
        if (progressDialog == null) { //Check to avoid creating another progress bar, just use 1
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Loading.."); //Title of progress bar
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL); //Style: can be horizontal or circle
            progressDialog.setIndeterminate(false); //Important line, if set true, progress is not updating
            progressDialog.setProgress(0);
            progressDialog.setMax(100);
            progressDialog.setCancelable(false); // Make progress bar can not be dismissed when click outside
            progressDialog.setCanceledOnTouchOutside(false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.toast:
                Map<String, Integer> countMap = getRestaurantTypesCount(RestaurantDetailFragment.getRestaurantList());
                String takeout = "Nhà hàng kiểu Takeout: " + countMap.get(RestaurantModel.TAKE_OUT);
                String sitdown = "Nhà hàng kiểu Sitdown: " + countMap.get(RestaurantModel.SIT_DOWN);
                String delivery = "Nhà hàng kiểu Delivery: " + countMap.get(RestaurantModel.DELIVERY);
                String displayText = takeout + "\n" + sitdown + "\n" + delivery;
                Toast.makeText(this, displayText, Toast.LENGTH_LONG).show();
                break;
            case R.id.run:
                showProgressBar();
                progress = 0;
                new Thread(longTask).start();
                break;

            default:
                break;
        }

        return true;
    }

    private void showProgressBar() {
        progressDialog.show();
    }

    private void doSomeLongWork(final int incr) throws InterruptedException {
        runOnUiThread(new Runnable() {
            public void run() {
                progress += incr;
                progressDialog.setProgress(progress / 100);
            }
        });
        Thread.sleep(250);    // should be something more useful!
    }

    private Runnable longTask = new Runnable() {
        public void run() {
            for (int i = 0; i < 20; i++) {
                try {
                    doSomeLongWork(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            runOnUiThread(new Runnable() {
                public void run() {
                    progressDialog.hide();
                }
            });
        }
    };
}
