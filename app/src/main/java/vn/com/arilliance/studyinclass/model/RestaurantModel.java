package vn.com.arilliance.studyinclass.model;

/**
 * Created by Hoang Tran on 3/7/2017.
 */

public class RestaurantModel {
    public static String TAKE_OUT = "Take-Out";
    public static String SIT_DOWN = "Sit-down";
    public static String DELIVERY = "delivery";


    private String name;
    private String address;
    private String type;
    private String discount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }
}
