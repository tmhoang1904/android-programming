package vn.com.arilliance.studyinclass.controller;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by Microsoft Windows on 16/03/2017.
 */

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    private RestaurantListFragment restaurantListFragment;
    private RestaurantDetailFragment restaurantDetailFragment;
    private SitdownRestaurantFragment sitdownRestaurantFragment;

    public ViewPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    public RestaurantListFragment getRestaurantListFragment() {
        return restaurantListFragment;
    }

    public void setRestaurantListFragment(RestaurantListFragment restaurantListFragment) {
        this.restaurantListFragment = restaurantListFragment;
    }

    public RestaurantDetailFragment getRestaurantDetailFragment() {
        return restaurantDetailFragment;
    }

    public void setRestaurantDetailFragment(RestaurantDetailFragment restaurantDetailFragment) {
        this.restaurantDetailFragment = restaurantDetailFragment;
    }

    public SitdownRestaurantFragment getSitdownRestaurantFragment() {
        return sitdownRestaurantFragment;
    }

    public void setSitdownRestaurantFragment(SitdownRestaurantFragment sitdownRestaurantFragment) {
        this.sitdownRestaurantFragment = sitdownRestaurantFragment;
    }

    public void notifyDataChanged(int position){
        switch (position){
            case 0:
                restaurantListFragment.notifyDataChanged();
                break;
            case 1:
                restaurantDetailFragment.notifyDataChanged();
                break;
            case 2:
                sitdownRestaurantFragment.notifyDataChanged();
                break;
        }
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                restaurantListFragment = new RestaurantListFragment();
                return restaurantListFragment;
            case 1:
                restaurantDetailFragment = new RestaurantDetailFragment();
                return restaurantDetailFragment;
            case 2:
                sitdownRestaurantFragment = new SitdownRestaurantFragment();
                return sitdownRestaurantFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
