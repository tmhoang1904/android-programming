package vn.com.arilliance.studyinclass.controller;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import vn.com.arilliance.studyinclass.MainActivity;
import vn.com.arilliance.studyinclass.R;
import vn.com.arilliance.studyinclass.model.RestaurantModel;

/**
 * Created by Microsoft Windows on 16/03/2017.
 */

public class RestaurantDetailFragment extends Fragment {

    private MainActivity mainActivity;

    private MainAdapter adapter;

    //Element Views
    private EditText txtAddr, txtName;
    private TextView txtMyId;
    private Button btnSave, btnList, btnFilter;
    private RadioGroup raTypesGroup, raDiscountGroup;
    private RecyclerView recyclerView;

    //Data holders
    private static List<RestaurantModel> restaurantList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedBundle) {
        super.onCreateView(inflater, parent, savedBundle);
        View view = inflater.inflate(R.layout.restaurant_detail_fragment, parent, false);
        initViews(view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainActivity = (MainActivity) context;

    }

    public static List<RestaurantModel> getRestaurantList() {
        return restaurantList;
    }

    public static void setRestaurantList(List<RestaurantModel> restaurantList) {
        RestaurantDetailFragment.restaurantList = restaurantList;
    }

    public void notifyDataChanged(){
        if (adapter != null){
            adapter.notifyDataSetChanged();
        }
    }

    private void initViews(View view) {
        txtAddr = (EditText) view.findViewById(R.id.addr);
        txtName = (EditText) view.findViewById(R.id.name);
//        txtMyId = (TextView) findViewById(R.id.txt_my_id);
        btnSave = (Button) view.findViewById(R.id.save);
        raTypesGroup = (RadioGroup) view.findViewById(R.id.radio_group_types_main);
        raDiscountGroup = (RadioGroup) view.findViewById(R.id.radio_group_discount_main);
        btnList = (Button) view.findViewById(R.id.list);
        btnFilter = (Button) view.findViewById(R.id.btn_filter);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        adapter = new MainAdapter(restaurantList, getContext());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);

        initViewActions();
    }

    private void initViewActions() {
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                String myId = txtMyId.getText().toString();
                String name = txtName.getText().toString();
                String addr = txtAddr.getText().toString();
                String type = "Type: " + getCheckedRadioGroup(raTypesGroup);
                String discount = "Discount: " + getCheckedRadioGroup(raDiscountGroup);
                String display = name + "\n" + addr + "\n" + type + "\n" + discount;
                saveRestaurant(name, addr, getCheckedRadioGroup(raTypesGroup), getCheckedRadioGroup(raDiscountGroup));
                Toast.makeText(getContext(), display, Toast.LENGTH_LONG).show();
            }
        });

        btnList.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (adapter != null) {
                    adapter.setRestaurantList(restaurantList);
                    adapter.notifyDataSetChanged();
                }

                if (restaurantList.size() <= 0) {
                    Toast.makeText(getContext(), "Chưa có nhà hàng nào được nhập!", Toast.LENGTH_LONG).show();
                } else {
                    String displayList = "";
                    for (int i = 0; i < restaurantList.size(); i++) {
                        displayList += restaurantList.get(i).getName() + "-" + restaurantList.get(i).getAddress();
                        if (i < restaurantList.size() - 1) {
                            displayList += "\n";
                        }
                    }
                    Toast.makeText(getContext(), displayList, Toast.LENGTH_LONG).show();
                }
            }
        });

        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String filter = getCheckedRadioGroup(raTypesGroup);
                List<RestaurantModel> filterList = new ArrayList<RestaurantModel>();
                for (RestaurantModel restaurant : restaurantList) {
                    if (restaurant.getType().equalsIgnoreCase(filter)) {
                        filterList.add(restaurant);
                    }
                }

                adapter.setRestaurantList(filterList);
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void saveRestaurant(String name, String addr, String type, String discount) {
        RestaurantModel restaurantModel = new RestaurantModel();
        restaurantModel.setName(name);
        restaurantModel.setAddress(addr);
        restaurantModel.setType(type);
        restaurantModel.setDiscount(discount);

        restaurantList.add(restaurantModel);
    }

    private String getCheckedRadioGroup(RadioGroup radioGroup) {
        String text = "";
        int checkedId = radioGroup.getCheckedRadioButtonId();
        RadioButton checkedRa = (RadioButton) getActivity().findViewById(checkedId);
        if (checkedRa != null) {
            text = checkedRa.getText().toString();
        }
        return text;
    }
}
