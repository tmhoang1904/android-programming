package vn.com.arilliance.studyinclass.controller;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import vn.com.arilliance.studyinclass.MainActivity;
import vn.com.arilliance.studyinclass.R;
import vn.com.arilliance.studyinclass.model.RestaurantModel;

/**
 * Created by Microsoft Windows on 16/03/2017.
 */

public class RestaurantListFragment extends Fragment {
    private MainActivity mainActivity;
    private MainAdapter adapter;
    private Handler handler = new Handler();

    //Element Views
    private RecyclerView recyclerView;

    //Data holders
    private List<RestaurantModel> restaurantList;
    private List<RestaurantModel> filterList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedBundle) {
        super.onCreateView(inflater, parent, savedBundle);
        View view = inflater.inflate(R.layout.restaurant_sit_down_fragment, parent, false);
        restaurantList = RestaurantDetailFragment.getRestaurantList();
        initViews(view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainActivity = (MainActivity) context;

    }

    public void notifyDataChanged(){
        if (adapter != null){
            adapter.notifyDataSetChanged();
        }
    }

    private void initViews(View view){
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        if (restaurantList != null){
            adapter = new MainAdapter(restaurantList, getContext());
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
            recyclerView.setAdapter(adapter);

//            handleListData(restaurantList);
        }
    }

    private void handleListData(final List<RestaurantModel> dataSource){
        new Thread(new Runnable() {
            @Override
            public void run() {
                filterList = new ArrayList<RestaurantModel>();
                if (dataSource != null) {
                    for (RestaurantModel model : dataSource) {
                        if (model.getType().equalsIgnoreCase("Sit-down")){
                            filterList.add(model);
                        }
                    }
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (adapter != null){
                                adapter.setRestaurantList(filterList);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    });
                }

            }
        }).start();
    }
}
