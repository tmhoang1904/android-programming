package vn.com.arilliance.studyinclass.controller;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import vn.com.arilliance.studyinclass.R;
import vn.com.arilliance.studyinclass.model.RestaurantModel;

/**
 * Created by Hoang Tran on 3/7/2017.
 */

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {

    private Context context;
    private List<RestaurantModel> restaurantList;

    public MainAdapter(List<RestaurantModel> list, Context context) {
        this.context = context;
        restaurantList = list;
    }

    public List<RestaurantModel> getRestaurantList() {
        return restaurantList;
    }

    public void setRestaurantList(List<RestaurantModel> restaurantList) {
        this.restaurantList = restaurantList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.restaurant_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String displayString = restaurantList.get(position).getName() + " - " + restaurantList.get(position).getAddress();

        holder.txt1.setText(displayString);

        if (restaurantList.get(position).getDiscount().equalsIgnoreCase("0%")){
            holder.imgRight.setVisibility(View.INVISIBLE);
            holder.imgLeft.setImageResource(R.drawable.ball_red);
        } else if (restaurantList.get(position).getDiscount().equalsIgnoreCase("25%")){
            holder.imgRight.setVisibility(View.VISIBLE);
            holder.imgRight.setImageResource(R.drawable.half_star_icon);
            holder.imgLeft.setImageResource(R.drawable.ball_yellow);
        } else if (restaurantList.get(position).getDiscount().equalsIgnoreCase("50%")){
            holder.imgRight.setVisibility(View.VISIBLE);
            holder.imgRight.setImageResource(R.drawable.full_star_icon);
            holder.imgLeft.setImageResource(R.drawable.ball_yellow);
        } else{
            holder.imgRight.setVisibility(View.VISIBLE);
            holder.imgRight.setImageResource(R.drawable.gift_icon);
            holder.imgLeft.setImageResource(R.drawable.ball_green);
        }
    }

    @Override
    public int getItemCount() {
        return restaurantList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgLeft, imgRight;
        TextView txt1;

        ViewHolder(View itemView) {
            super(itemView);
            txt1 = (TextView) itemView.findViewById(R.id.txt_name_restaurant_item);
            imgLeft = (ImageView) itemView.findViewById(R.id.img_restaurant_left_icon);
            imgRight = (ImageView) itemView.findViewById(R.id.img_restaurant_right_icon);
        }
    }
}
